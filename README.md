#Ada adatis100 implementation for stm32 nodes network

##Description

Our ADATIS-100 can be seen as a distributed synchronous
program through various microcontrollers. It's inspired of the
eponym game [tis-100](www.zachtronics.com/tis-100/).

##How it works

Assembly subprograms running in nodes. Our nodes will be stm32
micro-controllers and our edges will be USART links. Some nodes
will be responsible of the inputs, and some others of the
outputs. Basic available modes of computing nodes are "basic" and
"stack". 

##Spark partial proof

One of our goal is to prove partially (using Floyd's meaning) the
simulation of the assembly program in a node (aka the runtime).
Proof of the network (on the whole system) is not planned for the
moment. For now, no proof is done.

The begginning of the proof is available on the proof/ git branch.

##Requirements

The _big_ requirement is the gnat community compilation suit
(from adacore). Moreover, stm32f429 discovery kit is the only
board supported.

##Run it
```bash
export PATH=YOUR_GNAT_PATH/2018-arm-elf/bin:$PATH
gprbuild -p prj.gpr
```
