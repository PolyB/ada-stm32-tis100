------------------------------------------------------------------------------
--                                                                          --
--                     Copyright (C) 2015-2016, AdaCore                     --
--                                                                          --
--  Redistribution and use in source and binary forms, with or without      --
--  modification, are permitted provided that the following conditions are  --
--  met:                                                                    --
--     1. Redistributions of source code must retain the above copyright    --
--        notice, this list of conditions and the following disclaimer.     --
--     2. Redistributions in binary form must reproduce the above copyright --
--        notice, this list of conditions and the following disclaimer in   --
--        the documentation and/or other materials provided with the        --
--        distribution.                                                     --
--     3. Neither the name of the copyright holder nor the names of its     --
--        contributors may be used to endorse or promote products derived   --
--        from this software without specific prior written permission.     --
--                                                                          --
--   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    --
--   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      --
--   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  --
--   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   --
--   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, --
--   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       --
--   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  --
--   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  --
--   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    --
--   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  --
--   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   --
--                                                                          --
------------------------------------------------------------------------------

with Last_Chance_Handler;  pragma Unreferenced (Last_Chance_Handler);
--  The "last chance handler" is the user-defined routine that is called when
--  an exception is propagated. We need it in the executable, therefore it
--  must be somewhere in the closure of the context clauses.

with Basic.Execution;
with Basic.Graphics;
with Basic.State;
with Communicator;
with Defs;
with Input.Graphics;
with Program;
with Stack.Graphics;
with Stack;
with Peripherals;

procedure Main
is
   base : constant Program.Node := Program.InitialProgram;
   comm : Communicator.State;
begin
   Peripherals.Initialize (Defs.UP);
   Peripherals.COMs (Defs.UP).Start_Receiving (comm.msg_r'Unchecked_Access);
   case base.Kind is
      when Program.NODE_STACK =>
         declare
            S : Stack.Stack := base.stck;
         begin
            Stack.Graphics.Init;
            loop
               Stack.Graphics.Update (S);
            end loop;
         end;
      when Program.NODE_BASIC =>
         declare
            S : Basic.State.State;
         begin
            S.prog := base.prog;
            Basic.Graphics.Init (S.prog);
            loop
               Basic.Graphics.Update (S);
               Basic.Execution.Step (S, comm);
            end loop;
         end;
      when Program.NODE_INPUT =>
         Input.Graphics.Run;
   end case;
end Main;
