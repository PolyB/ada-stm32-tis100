with Basic.State; use Basic.State;
with Defs; use Defs;
with Communicator;

package Basic.Execution is
   type Result_Kind is (OK, WAIT);
   type StepResult (Kind : Result_Kind) is record
      case Kind is
         when OK => next_line : Line;
         when WAIT => null;
      end case;
   end record;
   procedure Step (St : in out State.State; Cst : in out Communicator.State);
private
   function Exec (St : in out State.State; Cst : in out Communicator.State)
      return StepResult;

   function NextLine (L : Line) return Line;
   function FirstNonEmpty (S : State.State; L : Line) return Line;
   function NextNonEmpty (S : State.State; L : Line) return Line;

   function ValueAdd (V1 : Value; V2 : Value) return Value;

   --  these are temporary
   function GetVal (St : State.State; S : Src; Cst : in out Communicator.State)
      return Maybe_Value;
   function WriteVal (S : in out State.State; V : Value; P : Channel;
                      Cst : in out Communicator.State) return Boolean;

end Basic.Execution;
