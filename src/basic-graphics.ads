with Basic.State; use Basic.State;
with Defs; use Defs;
with HAL.Bitmap; use HAL.Bitmap;

package Basic.Graphics is
   procedure Init (p : State.Program);
   procedure Update (S : State.State);
private
   type Prgm_Line_Str is access String;
   type Prgm_Lines is array (Line) of Prgm_Line_Str;

   function To_String (v : Instr) return String;
   function To_String (v : Channel) return String;
   function To_String (v : Src) return String;
end Basic.Graphics;
