package body GraphicsUtils is

   procedure Print_Char (BB : Any_Bitmap_Buffer;
                          C : Character;
                          P : Point) is
      use HAL;
   begin
      for H in 0 .. Text_height loop
         for W in 0 .. Text_width loop
            if (BMP_Fonts.Data (Text_font, C, H)
               and BMP_Fonts.Mask (Text_font, W)) /= 0
            then
               BB.Set_Pixel ((P.X + W, P.Y + H));
            end if;
         end loop;
      end loop;
   end Print_Char;

   procedure Print_Str (BB : Any_Bitmap_Buffer;
                        S : String;
                        p : Point) is
      CharNum : Natural := 0;
   begin
      for C of S loop
         Print_Char (BB, C, (P.X + CharNum * Text_width, P.Y));
         CharNum := CharNum + 1;
      end loop;
   end Print_Str;
end GraphicsUtils;
