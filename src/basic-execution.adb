package body Basic.Execution is
   procedure Step (St : in out State.State; Cst : in out Communicator.State) is
      R : constant StepResult := Exec (St, Cst);
   begin
      case R.Kind is
         when OK => St.ip := R.next_line;
         when WAIT => null;
      end case;
      Communicator.EndTick (Cst);
   end Step;

   function Exec (St : in out State.State; Cst : in out Communicator.State)
      return StepResult is
      Ip : constant Line := FirstNonEmpty (St, St.ip);
      Ins : constant Instr := St.prog (Ip);
   begin
      case Ins.Kind is
         when EMPTY =>
            --  SHOULD not happen, will be refactored
            return StepResult'(Kind => OK, next_line => NextNonEmpty (St, Ip));

         when ADD =>
            declare
               V : constant Maybe_Value := GetVal (St, Ins.what, Cst);
            begin
               case V.Kind is
                     when EMPTY_VALUE =>
                        return StepResult'(Kind => WAIT);
                     when SOME_VALUE =>
                        St.acc := ValueAdd (St.acc, V.v);
                        return StepResult'(Kind => OK,
                                           next_line => NextNonEmpty (St, Ip));
               end case;
            end;

         when SUB =>
            declare
               V : constant Maybe_Value := GetVal (St, Ins.what, Cst);
            begin
               case V.Kind is
                     when EMPTY_VALUE =>
                        return StepResult'(Kind => WAIT);
                     when SOME_VALUE =>
                        St.acc := ValueAdd (St.acc, -V.v);
                        return StepResult'(Kind => OK,
                                           next_line => NextNonEmpty (St, Ip));
               end case;
            end;

         when JRO =>
            declare
               V : constant Maybe_Value := GetVal (St, Ins.what, Cst);
            begin
               case V.Kind is
                     when EMPTY_VALUE =>
                        return StepResult'(Kind => WAIT);
                     when SOME_VALUE =>
                        declare
                           I : Integer := Ip + V.v;
                        begin
                           if I > Line'Last then
                              I := Line'Last;
                           end if;
                           if I < Line'First then
                              I := Line'First;
                           end if;
                        --  TODO
                           return StepResult'(Kind => OK, next_line => I);
                        end;
               end case;
            end;

         when JMP =>
            return StepResult'(Kind => OK, next_line => Ins.pos);

         when JEZ =>
            if St.acc = 0 then
               return StepResult'(Kind => OK, next_line => Ins.pos);
            else
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            end if;

         when JNZ =>
            if not (St.acc = 0) then
               return StepResult'(Kind => OK, next_line => Ins.pos);
            else
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            end if;

         when JGZ =>
            if St.acc > 0 then
               return StepResult'(Kind => OK, next_line => Ins.pos);
            else
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            end if;

         when JLZ =>
            if St.acc < 0 then
               return StepResult'(Kind => OK, next_line => Ins.pos);
            else
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            end if;

         when NOP =>
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));

         when SAV =>
               St.bak := St.acc;
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));

         when NEG =>
               St.acc := -St.acc;
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));

         when SWP =>
            declare
               tmp : constant Value := St.acc;
            begin
               St.acc := St.bak;
               St.bak := tmp;
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            end;

         when MOV =>
            if St.tmp.Kind = EMPTY_VALUE then
               --  we have to read a value
               declare
                  V : constant Maybe_Value := GetVal (St, Ins.from, Cst);
               begin
                  case V.Kind is
                     when EMPTY_VALUE =>
                        return StepResult'(Kind => WAIT);
                     when SOME_VALUE =>
                        St.tmp := Maybe_Value'(Kind => SOME_VALUE, v => V.v);
                  end case;
               end;
            end if;

            if WriteVal (St, St.tmp.v, Ins.to, Cst) then
               St.tmp := Maybe_Value'(Kind => EMPTY_VALUE);
               return StepResult'(Kind => OK,
                                  next_line => NextNonEmpty (St, Ip));
            else
               return StepResult'(Kind => WAIT);
            end if;

      end case;
   end Exec;

   function NextLine (L : Line) return Line is
   begin
      if L = Line'Last then
         return Line'First;
      else
         return L + 1;
      end if;
   end NextLine;

   function FirstNonEmpty (S : State.State; L : Line) return Line is
      Li : Line := L;
   begin
      while S.prog (Li).Kind = EMPTY loop
         Li := NextLine (Li);
      end loop;
      return Li;
   end FirstNonEmpty;

   function NextNonEmpty (S : State.State; L : Line) return Line is
   begin
      return FirstNonEmpty (S, NextLine (L));
   end NextNonEmpty;

   function ValueAdd (V1 : Value; V2 : Value) return Value is
      I : constant Integer := V1 + V2;
   begin
      case I is
         when Integer'First .. -999 => return -999;
         when 999 .. Integer'Last => return 999;
         when others => return I;
      end case;
   end ValueAdd;

   function GetVal (St : State.State; S : Src; Cst : in out Communicator.State)
      return Maybe_Value is
   begin
      case S.Kind is
         when SRC_IMM => return Maybe_Value'(Kind => SOME_VALUE, v => S.v);
         when SRC_PORT =>
            case S.p is
               when ACC => return Maybe_Value'(Kind => SOME_VALUE,
                                               v => St.acc);
               when NIL => return Maybe_Value'(Kind => SOME_VALUE, v => 0);
               --  TODO : read  the value
               --  when Port'Range => return Maybe_Value'(Kind => EMPTY_VALUE);
               when Port'Range => return Communicator.Recv (Cst);
            end case;
      end case;
   end GetVal;

   function WriteVal (S : in out State.State; V : Value; P : Channel;
                      Cst : in out Communicator.State) return Boolean is
   begin
      case P is
         when ACC => S.acc := V; return True;
         when NIL => null; return True;
         --  when Port'Range => return false; -- TODO
         when Port'Range => return Communicator.Send (Cst, V); -- TODO
      end case;
   end WriteVal;

end Basic.Execution;
