with GraphicsUtils;
with STM32.Board; use STM32.Board;
with HAL.Touch_Panel;       use HAL.Touch_Panel;
with LCD_Std_Out;

package body Input.Graphics is
   procedure Run is
      type RectMap is array (Area) of Rect;
      Rects : RectMap;
   begin
      Display.Initialize;
      Display.Set_Background (0, 0, 0);
      Display.Initialize_Layer (1, ARGB_4444);
      Touch_Panel.Initialize;

      declare
         buf : constant Any_Bitmap_Buffer := Display.Hidden_Buffer (1);
      begin

         buf.Set_Source (HAL.Bitmap.White);

         buf.Draw_Horizontal_Line ((0, 0), buf.Width);
         buf.Draw_Horizontal_Line ((2, 2), buf.Width - 4);
         buf.Draw_Horizontal_Line ((0, buf.Height - 1), buf.Width);
         buf.Draw_Horizontal_Line ((2, buf.Height - 3), buf.Width - 4);

         buf.Draw_Vertical_Line ((0, 0), buf.Height);
         buf.Draw_Vertical_Line ((2, 2), buf.Height - 4);
         buf.Draw_Vertical_Line ((buf.Width - 1, 0), buf.Height);
         buf.Draw_Vertical_Line ((buf.Width - 3, 2), buf.Height - 4);

         for AreaP in Area loop
            declare
               z : constant Zone := Areas (AreaP);
               r : constant Rect := GetRect (buf, z);
            begin
               PrettyBorder (buf, r);
               WriteText (buf, AreaP, r);
               Rects (AreaP) := r;
            end;
         end loop;
         Display.Update_Layer (1, Copy_Back => True);

      end;
      declare
         LastArea : AreaT := None;
         function GetArea (P : Point) return AreaT is
            function IsInside (P : Point; R : Rect) return Boolean is
                         (P.X >= R.Position.X
                 and then P.Y >= R.Position.Y
                 and then P.X <= R.Position.X + R.Width
                 and then P.Y <= R.Position.Y + R.Height);

         begin
            for A in Rects'Range loop
               if IsInside (P, Rects (A)) then
                  return A;
               end if;
            end loop;
            return None;
         end GetArea;
         subtype InputIndex is Integer range 1 .. 3;
         subtype InputCount is Integer range 0 .. 3;
         type Curr_Input is array (InputIndex) of NumArea;
         Curr_Val : Curr_Input;
         ICount : InputCount := 0;
         Negative : Boolean := False;

         procedure HandleClick (Pos : Area) is
         begin
            case Pos is
               when Current | Buffer => return;
               when '0' .. '9' =>
                  if ICount = InputCount'Last then
                     return;
                  end if;
                  ICount := ICount + 1;
                  Curr_Val (ICount) := Pos;
               when '<' =>
                  if ICount = InputCount'First then
                     return;
                  end if;
                  ICount := ICount - 1;
               when '-' =>
                  Negative := not Negative;
               when Send =>
                  --  TODO : update graphisms
                  --  TODO : send
                  ICount := InputCount'First;
                  Negative := False;
            end case;
            declare
               R : constant Rect := Rects (Current);
               P : constant Point := (R.Position.X + 2,
                  R.Position.Y + (R.Height - GraphicsUtils.Text_height) / 2);
               Str : String (1 .. 4) := "    ";
               function ToChar (C : NumArea) return Character is
                  (case C is
               when '0' => '0',
               when '1' => '1',
               when '2' => '2',
               when '3' => '3',
               when '4' => '4',
               when '5' => '5',
               when '6' => '6',
               when '7' => '7',
               when '8' => '8',
               when '9' => '9'
                  );
            begin
               if Negative then
                  Str (1) := '-';
               end if;
               for I in InputIndex'Range loop
                  if I <= ICount then
                     Str (1 + I) := ToChar (Curr_Val (I));
                  end if;
               end loop;
               LCD_Std_Out.Put (P.X, P.Y, Str);
            end;
            --  TODO

         end HandleClick;
      begin
         LCD_Std_Out.Set_Font (GraphicsUtils.Text_font);
         loop
            declare
               Points : constant TP_State := Touch_Panel.Get_All_Touch_Points;
            begin
               case Points'Length is
                  when 1 =>
                     declare
                        TouchArea : constant AreaT := GetArea (
                                                   (Points (Points'First).X,
                                                   Points (Points'First).Y));
                     begin
                        if TouchArea /= None and then
                           TouchArea /= LastArea
                        then
                           LastArea := TouchArea;
                        end if;
                     end;
                  when 0 =>
                     if LastArea /= None then
                        HandleClick (LastArea);
                        LastArea := None;
                     end if;
                  when others => null;
               end case;
            end;
         end loop;
      end;
   end Run;

   function MiddleOf (R : Rect; W : Integer; H : Integer) return Point is
   begin
      return (X => R.Position.X + (R.Width - W) / 2,
      Y => R.Position.Y + (R.Height - H) / 2);
   end MiddleOf;

   function GetRect (buf : Any_Bitmap_Buffer; X : ColId; Y : LineId)
      return Rect is
      Content_SizeW : constant Integer := (buf.Width - 6 - Gap) /
                                          (ColId'Last + 1);
      Content_SizeH : constant Integer := (buf.Height - 6 - Gap) /
                                          (LineId'Last + 1);
   begin
      return ((X => Content_SizeW * X + Gap + 4,
               Y => Content_SizeH * Y + Gap + 4),
                  Width => Content_SizeW - Gap - 2,
                  Height => Content_SizeH - Gap - 2);
   end GetRect;
   function GetRect (buf : Any_Bitmap_Buffer; Z : Zone) return Rect is
      Content_SizeW : constant Integer := (buf.Width - 6 - Gap) /
                                          (ColId'Last + 1);
      Content_SizeH : constant Integer := (buf.Height - 6 - Gap) /
                                          (LineId'Last + 1);
   begin
      return ((X => Content_SizeW * Z.X_min + Gap + 4,
               Y => Content_SizeH * Z.Y_min + Gap + 4),
                  Width => Content_SizeW * (1 + (Z.X_max - Z.X_min)) - Gap - 2,
                  Height => Content_SizeH * (1 + (Z.Y_max - Z.Y_min))
                              - Gap - 2);
   end GetRect;

   procedure PrettyBorder (buf : Any_Bitmap_Buffer; area : Rect) is
   begin
      buf.Draw_Horizontal_Line (area.Position - (1, 1), area.Width + 2);
      buf.Draw_Horizontal_Line (area.Position + (1, 1), area.Width - 2);

      buf.Draw_Horizontal_Line (area.Position + (0, area.Height)
                                            + (0, 1) - (1, 0), area.Width + 3);
      buf.Draw_Horizontal_Line (area.Position + (0, area.Height)
                                            - (0, 1) + (1, 0), area.Width - 1);

      buf.Draw_Vertical_Line (area.Position - (1, 1), area.Height + 2);
      buf.Draw_Vertical_Line (area.Position + (1, 1), area.Height - 2);

      buf.Draw_Vertical_Line (area.Position + (area.Width, 0) - (0, 1)
                                                    + (1, 0), area.Height + 3);
      buf.Draw_Vertical_Line (area.Position + (area.Width, 0) + (0, 1)
                                                    - (1, 0), area.Height - 1);
   end PrettyBorder;

   procedure WriteText (buf : Any_Bitmap_Buffer; A : Area; R : Rect) is
      procedure PrintMiddle (S : String) is
         P : constant Point := MiddleOf (R, S'Length *
                                             GraphicsUtils.Text_width,
                                             GraphicsUtils.Text_height);
      begin
         GraphicsUtils.Print_Str (buf, S, P);
      end PrintMiddle;
   begin
      case A is
         when '0' => PrintMiddle ("0");
         when '1' => PrintMiddle ("1");
         when '2' => PrintMiddle ("2");
         when '3' => PrintMiddle ("3");
         when '4' => PrintMiddle ("4");
         when '5' => PrintMiddle ("5");
         when '6' => PrintMiddle ("6");
         when '7' => PrintMiddle ("7");
         when '8' => PrintMiddle ("8");
         when '9' => PrintMiddle ("9");
         when Send => PrintMiddle ("SEND");
         when '-' => PrintMiddle ("-");
         when '<' => PrintMiddle ("<-");
         when Buffer => null;
         when Current => GraphicsUtils.Print_Char (buf, '>',
                  (R.Position.X + 1,
                     R.Position.Y + (R.Height - GraphicsUtils.Text_height) / 2)
                     );
      end case;
   end WriteText;
end Input.Graphics;
