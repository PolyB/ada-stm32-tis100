package Defs is

   subtype Value is Integer range -999 .. 999;
   type Channel is (ACC, NIL, UP, DOWN, LEFT, RIGHT, ANY, LAST);
   subtype Port is Channel range UP .. LAST;
   subtype Physical_Port is Channel range UP .. RIGHT;

   type Maybe_Value_Kind is (EMPTY_VALUE, SOME_VALUE);
   type Maybe_Value (Kind : Maybe_Value_Kind := EMPTY_VALUE) is record
      case Kind is
         when SOME_VALUE => v : Value;
         when EMPTY_VALUE => null;
      end case;
   end record;

end Defs;
