with HAL.Bitmap; use HAL.Bitmap;

package Input.Graphics is
   procedure Run;
   type AreaT is (None, '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-',
                  '<', Send, Current, Buffer);
   subtype Area is AreaT range '0' ..  Buffer;
   subtype NumArea is Area range '0' ..  '9';
   subtype LineId is Integer range 0 .. 4;
   subtype ColId is Integer range 0 .. 3;
   Gap : constant Integer := 3;
   type Zone is record
      X_min : ColId;
      X_max : ColId;
      Y_min : LineId;
      Y_max : LineId;
   end record;
   type ZoneMap is array (LineId, ColId) of Area;
   type RevZoneMap is array (Area) of Zone;

private
   function MiddleOf (R : Rect; W : Integer; H : Integer) return Point;
   function SingleZone (X : ColId; Y : LineId) return Zone is (X, X, Y, Y);
   procedure PrettyBorder (buf : Any_Bitmap_Buffer; area : Rect);
   Zones : constant ZoneMap := (('1', '2', '3', Buffer),
                                ('4', '5', '6', Buffer),
                                ('7', '8', '9', Buffer),
                                ('<', '0', '-', Buffer),
                                (Send, Send, Send, Current));
   Areas : constant RevZoneMap := ('1' => SingleZone (0, 0),
                                   '2' => SingleZone (1, 0),
                                   '3' => SingleZone (2, 0),
                                   '4' => SingleZone (0, 1),
                                   '5' => SingleZone (1, 1),
                                   '6' => SingleZone (2, 1),
                                   '7' => SingleZone (0, 2),
                                   '8' => SingleZone (1, 2),
                                   '9' => SingleZone (2, 2),
                                   '<' => SingleZone (0, 3),
                                   '0' => SingleZone (1, 3),
                                   '-' => SingleZone (2, 3),
                                   Current => SingleZone (3, 4),
                                   Buffer => (3, 3, 0, 3),
                                   Send => (0, 2, 4, 4)
                                  );

   function GetRect (buf : Any_Bitmap_Buffer; X : ColId; Y : LineId)
      return Rect;
   function GetRect (buf : Any_Bitmap_Buffer; Z : Zone) return Rect;
   procedure WriteText (buf : Any_Bitmap_Buffer; A : Area; R : Rect);

end Input.Graphics;
