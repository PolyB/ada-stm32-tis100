with Defs; use Defs;

package Basic.State is
   subtype Line is Integer range 1 .. 15;

   type Src_Kind is (SRC_PORT, SRC_IMM);
   type Src (Kind : Src_Kind := SRC_IMM) is record
      case Kind is
         when SRC_PORT => p : Channel;
         when SRC_IMM => v : Value;
      end case;
   end record;

   type Instr_Kind is (EMPTY, NOP, MOV, SWP, SAV, ADD, SUB, NEG, JMP, JEZ, JNZ,
                        JGZ, JLZ, JRO);
   type Instr (Kind : Instr_Kind := EMPTY) is record
      case Kind is
         when MOV                            => from : Src; to : Channel;
         when ADD | SUB | JRO                => what : Src;
         when JMP | JEZ | JNZ | JGZ | JLZ    => pos : Line;
         when others => null;
      end case;
   end record;

   type Program is array (Line) of Instr;

   type State is limited record
      prog : Program;
      ip : Line := 1;
      acc : Value := 0;
      bak : Value := 0;
      tmp : Maybe_Value := Maybe_Value'(Kind => EMPTY_VALUE);
   end record;
private

end Basic.State;
