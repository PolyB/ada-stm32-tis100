package body Peripherals is

   procedure Initialize_STMicro_UART (p : Physical_Port) is
   begin
      Enable_Clock (Transceivers (p).all);
      Enable_Clock (RX_Pins (p) & TX_Pins (p));

      Configure_IO
        (RX_Pins (p) & TX_Pins (p),
         (Mode           => Mode_AF,
          AF             => Transceivers_AF (p),
          Resistors      => Pull_Up,
          AF_Output_Type => Push_Pull,
          AF_Speed       => Speed_50MHz));
   end Initialize_STMicro_UART;

   procedure Initialize (p : Physical_Port) is
   begin
      Initialize_STMicro_UART (p);

      Disable (Transceivers (p).all);

      Set_Baud_Rate    (Transceivers (p).all, 9600);
      Set_Mode         (Transceivers (p).all, Tx_Rx_Mode);
      Set_Stop_Bits    (Transceivers (p).all, Stopbits_1);
      Set_Word_Length  (Transceivers (p).all, Word_Length_8);
      Set_Parity       (Transceivers (p).all, No_Parity);
      Set_Flow_Control (Transceivers (p).all, No_Flow_Control);

      Enable (Transceivers (p).all);
   end Initialize;

end Peripherals;
