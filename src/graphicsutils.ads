with HAL.Bitmap; use HAL.Bitmap;
with BMP_Fonts;

package GraphicsUtils is
   Text_font : constant BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
   Text_width : constant Positive := BMP_Fonts.Char_Width (Text_font);
   Text_height : constant Positive := BMP_Fonts.Char_Height (Text_font);
   procedure Print_Char (BB : Any_Bitmap_Buffer;
                         C : Character;
                         P : Point);
   procedure Print_Str (BB : Any_Bitmap_Buffer;
                         S : String;
                         p : Point);
end GraphicsUtils;
