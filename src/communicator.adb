with Peripherals;
with Ada.Synchronous_Task_Control;

package body Communicator is
   function Send (st : in out State; v : Value) return Boolean is
      Unused : Maybe_Value;
   begin
      if st.s = COMM then
         return False; -- ERROR
      end if;
      st.s := COMM;
      Unused := Send_i (Maybe_Value'(Kind => SOME_VALUE, v => v), st);
      return Recv_i (False, st);
   end Send;

   function Recv (st : in out State) return Maybe_Value is
      Unused : Boolean;
   begin
      if st.s = COMM then
         return Maybe_Value'(Kind => EMPTY_VALUE); -- ERROR
      end if;
      st.s := COMM;
      declare
         v : constant Maybe_Value := Send_i (Maybe_Value'(Kind => EMPTY_VALUE),
                                             st);
      begin
         case v.Kind is
            when SOME_VALUE => Unused := Recv_i (True, st);
            when EMPTY_VALUE => Unused := Recv_i (False, st);
         end case;
         return v;
      end;
   end Recv;

   procedure EndTick (st : in out State) is
      Unused1 : Maybe_Value;
      Unused2 : Boolean;
   begin
      if st.s = SLEEP then
         Unused1 := Send_i (Maybe_Value'(Kind => EMPTY_VALUE), st);
         Unused2 := Recv_i (False, st);
      end if;
      EndTick_i (st);
      st.s := SLEEP;
      st.tickNum := st.tickNum + 1;
   end EndTick;

   function Message_To_String (m : Message) return Message_String is
      s : Message_String := "******";
   begin
      case m.Kind is
         when SEND =>
            s (1) := 'S';
            case m.v.Kind is
               when EMPTY_VALUE =>
                  s (2) := 'N';
                  for i in 3 .. 6 loop
                     s (i) := '.';
                  end loop;
               when SOME_VALUE =>
                  s (2) := 'V';
                  if m.v.v < 0 then
                     s (3) := '-';
                  else
                     s (3) := '+';
                  end if;
                  declare
                     val : Value := abs m.v.v;
                  begin
                     for i in reverse 4 .. 6 loop
                        declare
                           d : constant Value := val mod 10;
                           ds : constant String := d'Image;
                        begin
                           s (i) := ds (2);
                           val := val / 10;
                        end;
                     end loop;
                  end;
            end case;
         when RECV =>
            s (1) := 'R';
            if m.b then
               s (2) := 'V';
            else
               s (2) := 'N';
            end if;
            for i in 3 .. 6 loop
               s (i) := '.';
            end loop;
         when M_END =>
            s (1) := 'E';
            for i in 2 .. 6 loop
               s (i) := '.';
            end loop;
      end case;
      return s;
   end Message_To_String;

   function String_To_Message (s : Message_String) return Message is
   begin
      case s (1) is
         when 'S' =>
            case s (2) is
               when 'V' =>
                  declare
                     m : Message := Message'(Kind => SEND,
                              v => Maybe_Value'(Kind => SOME_VALUE, v => 0));
                  begin
                     m.v.v := Integer'Value (s (4 .. 6));
                     case s (3) is
                        when '-' => m.v.v := -m.v.v;
                        when '+' => null;
                        when others => null; -- ERROR
                     end case;
                     return m;
                  end;
               when 'N' =>
                  return Message'(Kind => SEND,
                                  v => Maybe_Value'(Kind => EMPTY_VALUE));
               when others => null; -- ERROR
            end case;
         when 'R' =>
            declare
               m : Message := Message'(Kind => RECV, b => False);
            begin
               case s (2) is
                  when 'N' => m.b := False;
                  when 'V' => m.b := True;
                  when others => null; -- ERROR
               end case;
               return m;
            end;
         when 'E' =>
            return Message'(Kind => M_END);
         when others => null; -- ERROR
      end case;
      return Message'(Kind => M_END); -- ERROR
   end String_To_Message;

   function Send_Message (m : Message; no_ans : Boolean; st : in out State)
      return Message is
      s : constant Message_String := Message_To_String (m);
   begin
      Serial_Port.Set (st.msg_s, To => s);
      --  Send the message string (6 bytes), asynchronous.
      Peripherals.COMs (UP).Start_Sending (st.msg_s'Unchecked_Access);
      Ada.Synchronous_Task_Control.Suspend_Until_True (
            st.msg_s.Transmission_Complete);
      --  Recv the message of the other (6 bytes), blocking.
      if no_ans then
         return m;
      end if;
      Ada.Synchronous_Task_Control.Suspend_Until_True (
            st.msg_r.Reception_Complete);
      Peripherals.COMs (UP).Start_Receiving (st.msg_r'Unchecked_Access);
      return String_To_Message (Serial_Port.As_String (st.msg_r));
   end Send_Message;

   function Send_i (v : Maybe_Value; st : in out State) return Maybe_Value is
      m : constant Message := Send_Message (Message'(Kind => SEND, v => v),
            False, st);
   begin
      if m.Kind /= SEND then
         if not st.started then
            st.started := True;
            st.starting := True;
            return Maybe_Value'(Kind => EMPTY_VALUE);
         else
            null; -- ERROR
         end if;
      end if;
      return m.v;
   end Send_i;

   function Recv_i (b : Boolean; st : in out State) return Boolean is
      m : constant Message := Send_Message (Message'(Kind => RECV, b => b),
                                             st.starting, st);
   begin
      if st.starting then
         return False;
      end if;
      if m.Kind /= RECV then
         null; -- ERROR
      end if;
      return m.b;
   end Recv_i;

   procedure EndTick_i (st : in out State) is
      m : constant Message := Send_Message (Message'(Kind => M_END),
                                            False, st);
   begin
      if m.Kind /= M_END then
         null; -- ERROR
      end if;
      st.started := True;
      st.starting := False;
   end EndTick_i;

   --  procedure Wait_For_Start is

   --  begin
   --     null;
   --  end Wait_For_Start;
end Communicator;
