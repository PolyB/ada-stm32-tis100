with Basic.State;
with Defs;
with Stack;

package Program is
   type Node_Kind is (NODE_BASIC, NODE_STACK, NODE_INPUT);
   type Node (Kind : Node_Kind := NODE_STACK) is record
      case Kind is
         when NODE_BASIC => prog : Basic.State.Program;
         when NODE_STACK => stck : Stack.Stack;
         when NODE_INPUT => null;
      end case;
   end record;
   type PortUsage is array (Defs.Physical_Port) of Boolean;
   function InitialProgram return Node;
   function InitialPortUsage return PortUsage;
end Program;
