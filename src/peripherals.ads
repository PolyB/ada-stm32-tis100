------------------------------------------------------------------------------
--                                                                          --
--                 Copyright (C) 2015-2016, AdaCore                         --
--                                                                          --
--  Redistribution and use in source and binary forms, with or without      --
--  modification, are permitted provided that the following conditions are  --
--  met:                                                                    --
--     1. Redistributions of source code must retain the above copyright    --
--        notice, this list of conditions and the following disclaimer.     --
--     2. Redistributions in binary form must reproduce the above copyright --
--        notice, this list of conditions and the following disclaimer in   --
--        the documentation and/or other materials provided with the        --
--        distribution.                                                     --
--     3. Neither the name of the copyright holder nor the names of its     --
--        contributors may be used to endorse or promote products derived   --
--        from this software without specific prior written permission.     --
--                                                                          --
--   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    --
--   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      --
--   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  --
--   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   --
--   HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, --
--   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       --
--   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  --
--   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  --
--   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    --
--   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  --
--   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   --
--                                                                          --
------------------------------------------------------------------------------

with Serial_Port;          use Serial_Port;
with STM32.Device;         use STM32.Device;
with STM32.GPIO;           use STM32.GPIO;
with STM32.USARTs;         use STM32.USARTs;
with Ada.Interrupts;       use Ada.Interrupts;
with Ada.Interrupts.Names; use Ada.Interrupts.Names;
with Defs; use Defs;

package Peripherals is
   --  FIXME the controller USART2 doesn't seem to work

   type Transceiver_Acc is access all USART;
   type Transceiver_Arr is array (Physical_Port) of Transceiver_Acc;

   Transceivers : Transceiver_Arr :=
      (USART_1'Access, USART_3'Access, USART_2'Access, USART_6'Access);

   type Pin_Arr is array (Physical_Port) of GPIO_Point;

   TX_Pins : constant Pin_Arr :=
      (PA9, PB10, PA2, PC6);
   RX_Pins : constant Pin_Arr :=
      (PA10, PB11, PA3, PC7);

   type Transceiver_Interrupt_ID_Arr is array (Physical_Port) of Interrupt_ID;

   Transceivers_Interrupt_ID : constant Transceiver_Interrupt_ID_Arr :=
      (USART1_Interrupt, USART3_Interrupt, USART2_Interrupt, USART6_Interrupt);

   type Transceiver_AF_Arr is array (Physical_Port)
         of STM32.GPIO_Alternate_Function;

   Transceivers_AF : constant Transceiver_AF_Arr :=
      (GPIO_AF_USART1_7, GPIO_AF_USART3_7, GPIO_AF_USART2_7, GPIO_AF_USART6_8);

   COM_UP : aliased Serial_Port.Controller
      (Transceivers (UP), Transceivers_Interrupt_ID (UP));
   COM_DOWN : aliased Serial_Port.Controller
      (Transceivers (DOWN), Transceivers_Interrupt_ID (DOWN));
   COM_LEFT : aliased Serial_Port.Controller
      (Transceivers (LEFT), Transceivers_Interrupt_ID (LEFT));
   COM_RIGHT : aliased Serial_Port.Controller
      (Transceivers (RIGHT), Transceivers_Interrupt_ID (RIGHT));

   type COM_Arr is array (Physical_Port) of access Serial_Port.Controller;
   COMs : COM_Arr :=
      (COM_UP'Access, COM_DOWN'Access, COM_LEFT'Access, COM_RIGHT'Access);

   procedure Initialize_STMicro_UART (p : Physical_Port);

   procedure Initialize (p : Physical_Port);

end Peripherals;
