with Defs; use Defs;
package Stack is
   type Stack is private;
   function Empty return Stack;

   function Is_Full (S : Stack) return Boolean;
   function Is_Empty (S : Stack) return Boolean;

   function Pop (S : in out Stack) return Maybe_Value;
   procedure Push (S : in out Stack; V : Value);

   subtype Stack_Elem_Count is Integer range 0 .. 15;
   subtype Stack_Elem_Index is Stack_Elem_Count range 1 .. 15;

   function Read (S : Stack; I : Stack_Elem_Index) return Maybe_Value;
private
   type Stack_Content is array (Stack_Elem_Index) of Value;
   type Stack is record
      count : Stack_Elem_Count;
      content : Stack_Content;
   end record;
end Stack;
