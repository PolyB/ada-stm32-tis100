with Defs; use Defs;
with Serial_Port;

package Communicator is
   type Step is (SLEEP, COMM);
   type State is limited record
      tickNum : Integer := 0;
      s : Step := SLEEP;
      started : Boolean := False;
      starting : Boolean := False;
      msg_s : aliased Serial_Port.Message (Physical_Size => 6);
      msg_r : aliased Serial_Port.Message (Physical_Size => 6);
   end record;

   --  Each tick must call Send or Recv then EndTick,
   --  or only EndTick.
   function Send (st : in out State; v : Value) return Boolean;
   function Recv (st : in out State) return Maybe_Value;
   procedure EndTick (st : in out State);

private
   type Message_Kind is (SEND, RECV, M_END);
   type Message (Kind : Message_Kind := M_END) is record
      case Kind is
         when SEND => v : Maybe_Value;
         when RECV => b : Boolean;
         when M_END => null;
      end case;
   end record;
   subtype Message_String is String (1 .. 6);

   --  Serialization functions
   function Message_To_String (m : Message) return Message_String;
   function String_To_Message (s : Message_String) return Message;

   --  This function is called 3 times on each tick, it sends one message
   --  and get one answer
   function Send_Message (m : Message; no_ans : Boolean; st : in out State)
      return Message;

   --  These functions are called once on each tick
   function Send_i (v : Maybe_Value; st : in out State) return Maybe_Value;
   function Recv_i (b : Boolean; st : in out State) return Boolean;
   procedure EndTick_i (st : in out State);

   --  This procedure wait until all boards are ready
   --  procedure Wait_For_Start;
end Communicator;
