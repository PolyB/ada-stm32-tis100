with STM32.Board; use STM32.Board;
with BMP_Fonts;
with GraphicsUtils;

package body Basic.Graphics is
   Border_Top : constant Integer := 4;
   Border_Left : constant Integer := 4;
   Text_font : constant BMP_Fonts.BMP_Font := BMP_Fonts.Font12x12;
   Text_width : constant Positive := BMP_Fonts.Char_Width (Text_font);
   Text_height : constant Positive := BMP_Fonts.Char_Height (Text_font);

   Lines : Prgm_Lines;

   procedure Init (p : State.Program) is
   begin
      --  Initialize LCD
      Display.Initialize;
      Display.Set_Background (0, 0, 0);
      Display.Initialize_Layer (1, ARGB_8888);
      Display.Initialize_Layer (2, ARGB_8888);
      declare
         Buf : constant Any_Bitmap_Buffer := Display.Hidden_Buffer (1);
      begin
         Buf.Set_Source (HAL.Bitmap.Black);
         --  Text
         Buf.Set_Source (HAL.Bitmap.White);
         for L in Line'Range loop
            Lines (L) := new String'(To_String (p (L)));
            GraphicsUtils.Print_Str (Buf, L'Image, (Border_Left,
                                 Border_Top + L * Text_height));
            GraphicsUtils.Print_Char (Buf, ':',
                                      (Border_Left + (3 * Text_width),
                                      Border_Top + L * Text_height));
            GraphicsUtils.Print_Str (Buf, Lines (L).all,
                                     (Border_Left + 4 * Text_width,
                                      Border_Top + L * Text_height));
         end loop;
         --  Border
         Buf.Draw_Horizontal_Line ((0, 0), Buf.Width);
         Buf.Draw_Horizontal_Line ((2, 2), Buf.Width - 4);
         Buf.Draw_Horizontal_Line ((0, Buf.Height - 1), Buf.Width);
         Buf.Draw_Horizontal_Line ((2, Buf.Height - 3), Buf.Width - 4);

         Buf.Draw_Vertical_Line ((0, 0), Buf.Height);
         Buf.Draw_Vertical_Line ((2, 2), Buf.Height - 4);
         Buf.Draw_Vertical_Line ((Buf.Width - 1, 0), Buf.Height);
         Buf.Draw_Vertical_Line ((Buf.Width - 3, 2), Buf.Height - 4);

         Buf.Draw_Horizontal_Line ((2, 225), Buf.Width - 4);
         Buf.Draw_Horizontal_Line ((2, 227), Buf.Width - 4);

         --  border for registers
         Buf.Draw_Vertical_Line ((Buf.Width / 2 - 1, 227), Buf.Height - 229);
         Buf.Draw_Vertical_Line ((Buf.Width / 2 + 1, 227), Buf.Height - 229);

         GraphicsUtils.Print_Str (Buf, "ACC",
                  (Buf.Width / 4 - ((3 * Text_width) / 2), 250));
         GraphicsUtils.Print_Str (Buf, "BCK",
                  ((3 * (Buf.Width / 4)) - ((3 * Text_width) / 2), 250));

      end;

      Display.Update_Layer (1, Copy_Back => True);
   end Init;
   procedure Update (S : State.State) is
      Line_start : constant Point := (Border_Left + 4 * Text_width,
                              Border_Top + S.ip * Text_height);
      Buf : constant Any_Bitmap_Buffer := Display.Hidden_Buffer (2);

   begin
      Buf.Set_Source (HAL.Bitmap.Transparent);
      Buf.Fill;

      Buf.Set_Source (HAL.Bitmap.White);
      Buf.Fill_Rect ((Line_start, Lines (S.ip).all'Length * Text_width,
                     Text_height));

      GraphicsUtils.Print_Str (Buf, S.acc'Image,
                               (Buf.Width / 4 - (2 * Text_width), 270));
      GraphicsUtils.Print_Str (Buf, S.bak'Image,
                               (3 * (Buf.Width / 4) - (2 * Text_width), 270));

      Buf.Set_Source (HAL.Bitmap.Black);
      GraphicsUtils.Print_Str (Buf, Lines (S.ip).all, Line_start);

      Display.Update_Layer (2, Copy_Back => False);
      return;
   end Update;

   function To_String (v : Instr) return String is
   begin
      case v.Kind is
         when MOV =>
            return "MOV " & To_String (v.from) & ", " & To_String (v.to);
         when ADD | SUB | JRO =>
            return v.Kind'Image & " " & To_String (v.what);
         when JMP | JEZ | JNZ | JGZ | JLZ =>
            return v.Kind'Image & " @" & v.pos'Image;
         when NOP | SWP | SAV | NEG =>
            return v.Kind'Image;
         when EMPTY =>
            return "";
      end case;

   end To_String;

   function To_String (v : Src) return String is
   begin
      case v.Kind is
         when SRC_IMM => return v.v'Image;
         when SRC_PORT => return v.p'Image;
      end case;
   end To_String;

   function To_String (v : Channel) return String is
   begin
      return v'Image;
   end To_String;
end Basic.Graphics;
