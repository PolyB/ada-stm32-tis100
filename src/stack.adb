package body Stack is
   function Empty return Stack is
      S : Stack;
   begin
      S.count := 0;
      return S;
   end Empty;

   function Is_Full (S : Stack) return Boolean is
   begin
      return S.count = Stack_Elem_Count'Last;
   end Is_Full;

   function Is_Empty (S : Stack) return Boolean is
   begin
      return S.count = Stack_Elem_Count'First;
   end Is_Empty;

   function Pop (S : in out Stack) return Maybe_Value is
   begin
      if S.count = 0 then
         return Maybe_Value'(Kind => EMPTY_VALUE);
      else
         S.count := S.count - 1;
         return Maybe_Value'(Kind => SOME_VALUE, v => S.content (S.count + 1));
      end if;
   end Pop;

   procedure Push (S : in out Stack; V : Value) is
   begin
      if not (S.count = Stack_Elem_Count'Last) then
         S.count := S.count + 1;
         S.content (S.count) := V;
      end if;
   end Push;

   function Read (S : Stack; I : Stack_Elem_Index) return Maybe_Value is
   begin
      if I <= S.count then
         return Maybe_Value'(Kind => SOME_VALUE, v => S.content (I));
      else
         return Maybe_Value'(Kind => EMPTY_VALUE);
      end if;
   end Read;

end Stack;
