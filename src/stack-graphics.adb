with HAL.Bitmap; use HAL.Bitmap;
with STM32.Board; use STM32.Board;
with LCD_Std_Out;

with GraphicsUtils;

package body Stack.Graphics is

   procedure Init is
   begin
      Display.Initialize;
      Display.Set_Background (0, 0, 0);
      Display.Initialize_Layer (1, ARGB_8888);
      LCD_Std_Out.Set_Font (GraphicsUtils.Text_font);
      declare
         buf : constant Any_Bitmap_Buffer := Display.Hidden_Buffer (1);
      begin
         buf.Set_Source (HAL.Bitmap.Black);

         buf.Set_Source (HAL.Bitmap.White);
         buf.Draw_Horizontal_Line ((0, 0), buf.Width);
         buf.Draw_Horizontal_Line ((2, 2), buf.Width - 4);
         buf.Draw_Horizontal_Line ((0, buf.Height - 1), buf.Width);
         buf.Draw_Horizontal_Line ((2, buf.Height - 3), buf.Width - 4);

         buf.Draw_Vertical_Line ((0, 0), buf.Height);
         buf.Draw_Vertical_Line ((2, 2), buf.Height - 4);
         buf.Draw_Vertical_Line ((buf.Width - 1, 0), buf.Height);
         buf.Draw_Vertical_Line ((buf.Width - 3, 2), buf.Height - 4);

         buf.Draw_Horizontal_Line ((2, 225), buf.Width - 4);
         buf.Draw_Horizontal_Line ((2, 227), buf.Width - 4);

         buf.Fill_Rect (((55, 265 - (GraphicsUtils.Text_height + 1)),
            GraphicsUtils.Text_width * 10, GraphicsUtils.Text_height));
         GraphicsUtils.Print_Str (buf, "STACK NODE", (55, 265));
         buf.Fill_Rect (((55, 265 + (GraphicsUtils.Text_height + 1)),
            GraphicsUtils.Text_width * 10, GraphicsUtils.Text_height));
      end;

      Display.Update_Layer (1, Copy_Back => True);
   end Init;

   procedure Update (p : Stack) is
      buf : constant Any_Bitmap_Buffer := Display.Hidden_Buffer (1);
      Xpos : constant Integer := ((buf.Width - 4)
                                - 4 * GraphicsUtils.Text_width) / 2;
   begin
      for I in Stack_Elem_Index'Range loop
         declare
            Ypos : constant Integer := 8 + I * (GraphicsUtils.Text_height);
            Val : constant Maybe_Value := Read (p, I);
         begin
            case Val.Kind is
               when EMPTY_VALUE =>
                  LCD_Std_Out.Put (Xpos, Ypos, " ---");
               when SOME_VALUE =>
                  LCD_Std_Out.Put (Xpos, Ypos, Val.v'Image);
            end case;
         end;
      end loop;
      null;
   end Update;

end Stack.Graphics;
