#!/usr/bin/env python3
import re
import sys

lines=[]

def format_instruction(instr, entries):
    parameters = [", {} => {}".format(k, entries[k]) for k in entries]
    return "Basic.State.Instr'(Kind => Basic.State.{}{})".format(instr, ''.join(parameters));

def mksrc(src):
    if src == 'ACC':
        return '(Basic.State.SRC_PORT, Defs.ACC)'
    if src == 'NIL':
        return '(Basic.State.SRC_PORT, Defs.NIL)'
    if src == 'LEFT':
        return '(Basic.State.SRC_PORT, Defs.LEFT)'
    if src == 'RIGHT':
        return '(Basic.State.SRC_PORT, Defs.RIGHT)'
    if src == 'UP':
        return '(Basic.State.SRC_PORT, Defs.UP)'
    if src == 'DOWN':
        return '(Basic.State.SRC_PORT, Defs.DOWN)'
    if src == 'ANY':
        return '(Basic.State.SRC_PORT, Defs.ANY)'
    if src == 'LAST':
        return '(Basic.State.SRC_PORT, Defs.LAST)'
    return "(Basic.State.SRC_IMM, {})".format(src)

def mkdst(dst):
    if dst == 'ACC':
        return 'Defs.ACC'
    if dst == 'NIL':
        return 'Defs.NIL'
    if dst == 'LEFT':
        return 'Defs.LEFT'
    if dst == 'RIGHT':
        return 'Defs.RIGHT'
    if dst == 'UP':
        return 'Defs.UP'
    if dst == 'DOWN':
        return 'Defs.DOWN'
    if dst == 'ANY':
        return 'Defs.ANY'
    if dst == 'LAST':
        return 'Defs.LAST'
    print("bad destination : ", dst, file=sys.stderr)
    sys.exit(1)

def mkjmp(label):
    for line, (linelabel, _) in enumerate(lines):
        if linelabel == label:
            return str(line + 1)
    print("cannot find label : ", label, file=sys.stderr)
    sys.exit(1)


def mkinstr(content):
    if content == '':
        return format_instruction("EMPTY", {})
    if content == 'SWP':
        return format_instruction("SWP", {})
    if content == 'NOP':
        return format_instruction("NOP", {})
    if content == 'SAV':
        return format_instruction("SAV", {})
    if content == 'NEG':
        return format_instruction("NEG", {})

    match = re.match(r'ADD\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("ADD", {'what' : mksrc(match.group(1))})

    match = re.match(r'SUB\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("SUB", {'what' : mksrc(match.group(1))})

    match = re.match(r'JRO\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JRO", {'what' : mksrc(match.group(1))})

    match = re.match(r'JMP\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JMP", {'pos' : mkjmp(match.group(1))})

    match = re.match(r'JEZ\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JEZ", {'pos' : mkjmp(match.group(1))})

    match = re.match(r'JNZ\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JNZ", {'pos' : mkjmp(match.group(1))})

    match = re.match(r'JGZ\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JGZ", {'pos' : mkjmp(match.group(1))})

    match = re.match(r'JLZ\s+(.*)', content, re.M | re.I)
    if match:
        return format_instruction("JLZ", {'pos' : mkjmp(match.group(1))})

    match = re.match(r'MOV\s+(.*)\s* \s*(.*)', content, re.M | re.I)
    if match:
        return format_instruction("MOV", {'from' : mksrc(match.group(1)), 'to' : mkdst(match.group(2))})

    print("bad instruction : ", content, file=sys.stderr)
    sys.exit(1)

def geninstrentry(v):
    line,(label,content) = v
    if content is None:
        content =''
    return "         {} => {}".format(line+1, mkinstr(content.strip()))

uses = []
def tryUse(v):
    match = re.match(r'!use (.*)', v, re.M | re.I)
    if match:
        uses.append(match.group(1))
        return True
    return False


# -- BEGIN OF SCRIPT
nodetype = sys.stdin.readline().strip()
if nodetype == '@BASIC':
    for line in sys.stdin:
        use = tryUse(line)
        if not use:
            match = re.match(r'((.*):)?(.*)', line, re.M | re.I)
            label = None if match.group(2) is None else match.group(2).strip()
            text = '' if match.group(3) is None else match.group(3).strip()
            lines.append((match.group(2), match.group(3)))

    if len(lines) > 15:
        print("too much lines, 15 lines allowed", file=sys.stderr)
        sys.exit(1)

    # add empty lines at the end
    for x in range (len(lines), 15):
        lines.append(('',''))

    res = list(map(geninstrentry, enumerate(lines)))

    print("package body Program is")
    print("   function InitialProgram return Node is")
    print("   begin")
    print("      return Node'(Kind => NODE_BASIC, prog => (")
    print(',\n'.join(res))
    print("      ));")
    print("   end InitialProgram;")
elif nodetype == '@STACK':
    for line in sys.stdin:
        use = tryUse(line)
        if not use:
            print("bad line : ", line, file=sys.stderr)
            sys.exit(1)
    print("package body Program is")
    print("   function InitialProgram return Node is")
    print("   begin")
    print("      return Node'(Kind => NODE_STACK, stck => (Stack.Empty)")
    print("      );")
    print("   end InitialProgram;")
elif nodetype == '@INPUT':
    for line in sys.stdin:
        use = tryUse(line)
        if not use:
            print("bad line : ", line, file=sys.stderr)
            sys.exit(1)
    print("package body Program is")
    print("   function InitialProgram return Node is")
    print("   begin")
    print("      return Node'(Kind => NODE_INPUT);")
    print("   end InitialProgram;")


print("   function InitialPortUsage return PortUsage is")
print("   begin")
print("      return (");
print(',\n'.join("         Defs.{} => {}".format(s, (s in uses)) for s in ["UP", "DOWN", "LEFT", "RIGHT"]))
print("      );")
print("   end InitialPortUsage;")
print("end Program;")
