.PHONY:flash obj/main src/program.adb

obj/main: src/program.adb
	gprbuild -P prj.gpr

src/program.adb: input.tis100
	./gen_program.py <$^ >$@

obj/main.hex: obj/main
	arm-eabi-objcopy -O binary $^ $@
flash: obj/main.hex
	st-flash --reset write $^ 0x8000000

clean:
	gprclean prj.gpr
